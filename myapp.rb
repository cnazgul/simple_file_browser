﻿# myapp.rb
require 'sinatra'
require 'slim'
require 'sass'

$root_path = Dir.pwd
$root_path_left = [$root_path]
$root_path_right = [$root_path]

def root_path(array_path)
	path = ""
	array_path.each do |a|
		path = path + a
	end	
	array_path = nil
	path		
end

get '/' do
	$left = Dir.new($root_path)	
	$right = Dir.new($root_path)
	$view_path_left = $root_path
	$view_path_right = $root_path
	
	$root_path_left.clear
	$root_path_right.clear
	$root_path_left = [$root_path]
	$root_path_right = [$root_path]
	
	$size_l = 0
	$size_r = 0
	$count_l = 0
	$count_r = 0	
	
	slim :index
end

get "/path_right" do
	$size_r = 0
	$count_r = 0
	if (params[:path_right].to_s == "..".to_s)
		$root_path_right.pop
	else
		$root_path_right << "/" + params[:path_right]				
	end	

	$right = Dir.new(root_path($root_path_right))			
	$view_path_right = root_path($root_path_right)
	
	slim :path_right
end

get "/path_left" do
	$size_l = 0
	$count_l = 0
	if (params[:path_left].to_s == "..".to_s)
		$root_path_left.pop
	else
		$root_path_left << "/" + params[:path_left]					
	end	

	$left = Dir.new(root_path($root_path_left))		
	$view_path_left = root_path($root_path_left)
	
	slim :path_left
end